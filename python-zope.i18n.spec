%global _empty_manifest_terminate_build 0
Name:		python-zope.i18n
Version:	4.7.0
Release:	2
Summary:	Zope Internationalization Support
License:	ZPLv2.1
URL:		https://github.com/zopefoundation/zope.i18n
Source0:	https://files.pythonhosted.org/packages/f8/95/48cb15ee969bb38406edc27957600874684783a615a814541e9576378498/zope.i18n-4.7.0.tar.gz
BuildArch:	noarch


%description
This package implements several APIs related to internationalization and
localization.
* Locale objects for all locales maintained by the ICU project.
* Gettext-based message catalogs for message strings.

%package -n python3-zope.i18n
Summary:	Zope Internationalization Support
Provides:	python-zope.i18n
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-zope.i18n
This package implements several APIs related to internationalization and
localization.
* Locale objects for all locales maintained by the ICU project.
* Gettext-based message catalogs for message strings.

%package help
Summary:	Development documents and examples for zope.i18n
Provides:	python3-zope.i18n-doc
%description help
This package implements several APIs related to internationalization and
localization.
* Locale objects for all locales maintained by the ICU project.
* Gettext-based message catalogs for message strings.

%prep
%autosetup -n zope.i18n-4.7.0

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-zope.i18n -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Aug 02 2022 fushanqing <fushanqing@kylinos.cn> - 4.7.0-2
- Unified license name specification

* Sun Jun 28 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
